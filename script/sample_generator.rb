#!/usr/bin/env ruby

fd = File.open('pi_file.txt', 'a')
Signal.trap("INT") {
	puts ""
	puts "-- Closing..."
	fd.close
	puts "-- bye."
	exit
}

loop do 
	fd.puts Time.now.to_s
	fd.fsync
	print '.'

	sleep 0.01
end