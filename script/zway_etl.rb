#!/usr/bin/env ruby
require 'optparse'
require 'debugger' if ENV['DEBUG']
require File.join('./', File.dirname(__FILE__), '..', 'lib', 'zway_etl.rb')

cfg = {}
OptionParser.new do |opts|
  opts.banner = "Usage: zway_etl --source=FILE --destination=FILE"

  opts.on("-s", "--source SOURCE_FILE", "Source File") do |v|
    cfg[:source] = v
  end

  opts.on("-d", "--destination DESTINATION_FILE", "Destination File") do |v|
    cfg[:destination] = v
  end

  opts.on("-m", "--monitor", "Monitor for changes") do |v|
  	cfg[:monitor] = v
  end
end.parse!

src = File.open(cfg[:source], 'r')
dest = File.open(cfg[:destination], 'a')
monitor = !!cfg[:monitor]
lt = ZwayETL.new(src, dest)

continue = true
Signal.trap("INT") {
	continue = false
	src.close
	dest.close
	print "Finito !\n"	
}	

require 'date'
DATE_MATCH = /[-:\d\s]+/
VAL_MATCH = /[\d\.]+$/
TEMP_MATCH = /SETDATA\sdevices.2.instances.0.commandClasses.49.data.1.val/

while continue
	src.fsync
		
	lt.e ->(line) {
		line =~ TEMP_MATCH &&
		line !~ /Empty/
	}
	lt.t ->(line) {
		time = line.match(DATE_MATCH)[0]
		time = DateTime.strptime(time, "%Y-%m-%d %H:%M").to_time.to_i * 1000
		val = line.match(VAL_MATCH)[0]
		line = time.to_s + ',' + val
	}
	lt.l
	lt.finalize

	dest.fsync

	print '.'

	if monitor
		sleep 3
	else #run in loop
		break
	end
end
