class ZwayETL
	POSITION_INFO_PTR = 'tmp/curr-pos'
	
	def initialize(source, destination)
		@src = source		
		@src.pos = read_position
		@dest = destination
		@new_lines = []
	end
	
	def read_position
		pos = File.open(POSITION_INFO_PTR, 'a+').read.to_i
		# when pos is bigger than src file in bytes
		# we assume that src was cleared
		@current_position = if pos > @src.stat.size
			0
		else
			pos
		end
	end

	def extract(proc = nil)
		@src.each_line do |line|
			if proc
				@new_lines << line if proc.call(line)
			end
		end
	end

	def transform(proc = nil)
		@new_lines.map! do |line|
			if proc
				line = proc.call(line)
			end			
		end
	end

	def load
		@new_lines.each do |line|
			@dest.puts line
		end
	end

	def finalize
		update_pos
		@new_lines = []
	end

	alias_method :e, :extract
	alias_method :t, :transform
	alias_method :l, :load	

	private
		def update_pos
			@current_position = @src.pos
			File.write(POSITION_INFO_PTR, @current_position)
		end

end
