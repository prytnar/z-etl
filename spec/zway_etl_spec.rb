require 'minitest/autorun'
require 'tempfile'
require 'debugger' if ENV['DEBUG']

require File.join('./', File.dirname(__FILE__), '..', 'lib', 'zway_etl.rb')

FILE_CONTENT = <<-STR
temp 12
hum 50
temp 12
hum 50
temp 12
hum 50
STR

describe ZwayETL do
	module Accessors
		refine ZwayETL do
			attr_accessor :new_lines, :current_position
		end
	end
	using Accessors
	ZwayETL.const_set('POSITION_INFO_PTR', 'tmp/curr-pos-test')

	before do
		@src = Tempfile.new('tsts-src')
		@dest = Tempfile.new('tsts-dest')
		@src.write(FILE_CONTENT)
		@src.rewind
	end

	after do
		File.unlink(ZwayETL::POSITION_INFO_PTR)
	end

	describe 'initialization' do
		it 'should reset position if file was cleared' do
			File.open(ZwayETL::POSITION_INFO_PTR, 'w') do |fd|
				fd.puts @src.stat.size + 1
			end

			@lt = ZwayETL.new(@src, @dest)
			assert @lt.current_position == 0
		end

		it "should create file with position info if not exists" do
			@lt = ZwayETL.new(@src, @dest)			
			assert @lt.current_position == 0
		end
	end

	describe 'functionalities' do	
		before do
			@lt = ZwayETL.new(@src, @dest)
		end

		it "should open file with position info with non zero value" do
			@lt.extract
			@lt.finalize
			assert @lt.current_position > 0
		end

		it "should extract lines for which condition function returns true" do
			@lt.extract(->(line){ line =~ /temp/ })
			assert @lt.new_lines.size == 3
		end

		it "should transform to data for chart" do
			@lt.new_lines = [
				"1402331846	23",
				"1402331946	24"
			]
			@lt.transform ->(line) { line.gsub!(/\s/, ';')}
			assert_equal @lt.new_lines, [
				"1402331846;23",
				"1402331946;24"
			]
		end

		it "should load collected lines to result file" do
			@lt.extract(->(line){ line =~ /temp/ })
			initial_pos = @dest.pos
			@lt.load

			assert @dest.pos != initial_pos
		end

		it "should update index on load" do
			@lt.extract
			@lt.finalize
			assert @lt.current_position > 0
		end

		it 'should clear new_lines' do
			# missing test	
		end
	end
end
